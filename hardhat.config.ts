import dotenv from "dotenv";
import "@nomicfoundation/hardhat-toolbox";

// Pulls in the .env file so it can be accessed from process.env. No path as .env is in root, the default location
dotenv.config();

const PRIVATE_KEY = process.env.PRIVATE_KEY

const config: any = {
  solidity: {
    compilers: [
      {
        version: "0.8.9"
      },
    ],
  },
  networks: {
    /*     binance: {
          url: process.env.BINANCE_MAINNET_URL,
          accounts: [PRIVATE_KEY],
        }, */
    binanceTestnet: {
      url: process.env.BINANCE_TESTNET_URL,
      accounts: [PRIVATE_KEY],
    }
  },
  etherscan: {
    apiKey: process.env.ETHERSCAN_API_KEY,
  },

};

export default config;
